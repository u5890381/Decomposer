import java.util.concurrent.TimeUnit;

public class Prime {
	public void decompose(long number) {
		System.out.println("Number: " + number);
		long test = 1;
		//You must put your code here
		System.out.println("test  : " + test);
	}
	
	public static void main(String[] args) {
		Prime p = new Prime();
        long startTime = System.nanoTime();
        p.decompose(12);
		p.decompose(1573);
		p.decompose(983l*991l);
		p.decompose(1258756456477765463l);
        long difference = System.nanoTime() - startTime;
        System.out.println("Elapsed Time: " + difference/1000000000.);	
	}
}

/* Out put
Number: 12
2 2 3 test  : 12
Number: 1573
11 11 13
test  : 1573
Number: 974153
983 991
test  : 974153
Number: 1258756456477765463
1258756456477765463
test  : 1258756456477765463
Elapsed Time: ....
*/

